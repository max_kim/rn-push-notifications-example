import { notificationsActions } from '../redux/notifications';
import { getNotificationsEntityHandler } from './getNotificationsEntityHandler';

export const getDeviceTokenHandler = () =>
  getNotificationsEntityHandler(notificationsActions.setToken);
