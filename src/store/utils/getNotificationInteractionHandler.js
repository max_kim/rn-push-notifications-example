import { Platform } from 'react-native';

import { notificationsActions } from '../redux/notifications';
import { getNotificationsEntityHandler } from './getNotificationsEntityHandler';

export const getNotificationInteractionHandler = () => {
  const interactionHandler = getNotificationsEntityHandler(
    notificationsActions.setCurrentNotification,
  );

  return (notification) => {
    const messageId = Platform.select({
      ios: notification?.data['gcm.message_id'],
      android: notification?.data?.messageId,
    });

    return interactionHandler(messageId || null);
  };
};
