import { notificationsActions } from '../redux/notifications';
import { getNotificationsEntityHandler } from './getNotificationsEntityHandler';

export const getSaveNotificationToStoreHandler = () =>
  getNotificationsEntityHandler(notificationsActions.addNotification);
