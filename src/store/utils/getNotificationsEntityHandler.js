import { getStore } from '../index';

const {
  store: { dispatch },
  persistor,
} = getStore();

export const getNotificationsEntityHandler = (action) => {
  let unsubscribePersistor;

  const dispatchNotificationsAction = (notificationEntity) => {
    dispatch(action(notificationEntity));

    unsubscribePersistor && unsubscribePersistor();
  };

  return async (notificationEntity) => {
    if (persistor) {
      const { bootstrapped } = persistor.getState();
      if (!bootstrapped) {
        unsubscribePersistor = persistor.subscribe(() =>
          dispatchNotificationsAction(notificationEntity),
        );

        return;
      }
    }

    dispatchNotificationsAction(notificationEntity);
  };
};
