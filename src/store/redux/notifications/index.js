import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  list: [],
  current: null,
  token: null,
};

export const notificationsSlice = createSlice({
  name: 'notifications',
  initialState,
  reducers: {
    addNotification: (state, action) => {
      state.list = [action.payload, ...state.list];
    },
    removeNotification: (state, action) => {
      state.list = state.list.filter(
        (notification) => notification.messageId !== action.payload,
      );
    },
    clearNotificationsList: (state) => {
      state.list = [];
      state.current = null;
    },
    setCurrentNotification: (state, action) => {
      state.current = action.payload;
    },
    setToken: (state, action) => {
      state.token = action.payload;
    },
  },
});

export const notificationsActions = notificationsSlice.actions;

export const notifications = notificationsSlice.reducer;
