import { createSelector } from 'reselect';

const notificationsListSelector = (store) => store.notifications.list;

export const getNotificationsList = createSelector(
  notificationsListSelector,
  (list) => list,
);

const currentNotificationSelector = (state) => state.notifications.current;

export const getCurrentNotification = createSelector(
  [notificationsListSelector, currentNotificationSelector],
  (list, current) =>
    list.find((notification) => notification.messageId === current),
);
