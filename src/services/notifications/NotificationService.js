import { Platform } from 'react-native';
import messaging from '@react-native-firebase/messaging';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import PushNotification from 'react-native-push-notification';

export class NotificationService {
  defaultChannelId = 'fcm_fallback_notification_channel';

  constructor({
    notificationsHandler,
    deviceTokenHandler,
    interactionHandler,
  }) {
    this.messaging = messaging();

    this.notificationsHandler = notificationsHandler;

    this.interactionHandler = interactionHandler;

    this.deviceTokenHandler = deviceTokenHandler;

    this.configure();
  }

  handleDeviceToken = (token) => {
    this.deviceTokenHandler && this.deviceTokenHandler(token);
  };

  handleUserInteraction = (message) => {
    this.interactionHandler && this.interactionHandler(message);
  };

  subscribeOnTokenRefresh = () => {
    return this.messaging.onTokenRefresh(this.handleDeviceToken);
  };

  getToken = async () => {
    const token = await this.messaging.getToken();
    console.log('#########');
    console.log('getToken - token:', token);
    console.log('#########');
    return token;
  };

  requestPermission = async () => {
    return await this.messaging.requestPermission();
  };

  subscribeToTopic = async (topicId) => {
    await this.messaging.subscribeToTopic(topicId).then(() => {
      console.log('#########');
      console.log(`Subscribed to topic - "${topicId}"!`);
      console.log('#########');
    });
  };

  unsubscribeFromTopic = async (topicId) => {
    await this.messaging.unsubscribeFromTopic(topicId).then(() => {
      console.log('#########');
      console.log(`Unsubscribed fom the topic - "${topicId}"!`);
      console.log('#########');
    });
  };

  createDefaultChannel = () => {
    if (Platform.OS !== 'android') {
      return true;
    }

    PushNotification.channelExists(this.defaultChannelId, (exists) => {
      if (!exists) {
        PushNotification.createChannel(
          {
            channelId: this.defaultChannelId,
            channelName: 'Default notifications channel',
          },
          (created) => console.log(`createChannel returned '${created}'`),
        );
      }
    });
  };

  showLocalNotification = (message) => {
    PushNotification.localNotification({
      id: Platform.select({
        ios: message.messageId,
        default: undefined,
      }),
      channelId: this.defaultChannelId,
      message: message.notification.body,
      title: message.notification.title,
      bigPictureUrl: Platform.select({
        android: message.notification.android?.imageUrl,
        default: undefined,
      }),
      userInfo: {
        ...message.data,
        messageId: message.messageId,
        picture: Platform.select({
          android: message.notification.android?.imageUrl,
          ios: message.data.fcm_options?.image,
        }),
      },
    });
  };

  handleRemoteMessage = (message, showPopup = true) => {
    this.notificationsHandler && this.notificationsHandler(message);
    showPopup && this.showLocalNotification(message);
  };

  registerBackgroundMessageHandler = () => {
    this.messaging.setBackgroundMessageHandler(async (message) =>
      this.handleRemoteMessage(message, true),
    );
  };

  subscribeOnForegroundMessages = (showPopup = true) =>
    this.messaging.onMessage(async (message) =>
      this.handleRemoteMessage(message, showPopup),
    );

  onRegister = (token) => {
    console.log('#########');
    console.log('onRegister - token:', token);
    console.log('#########');
  };

  onNotification = (notification) => {
    console.log('#########');
    console.log('onNotification - notification:', notification);
    console.log('#########');

    // process the notification
    if (notification.userInteraction) {
      this.handleUserInteraction(notification);
    }

    // (required) Called when a remote is received or opened, or local notification is opened
    notification.finish(PushNotificationIOS.FetchResult.NoData);
  };

  onAction(notification) {
    console.log('#########');
    console.log('onAction - action:', notification.action);
    console.log('onAction - notification:', notification);
    console.log('#########');

    // process the action
  }

  onRegistrationError(err) {
    console.error(err.message, err);
  }

  configure = async () => {
    await this.messaging.requestPermission();

    if (!this.messaging.isDeviceRegisteredForRemoteMessages) {
      await this.messaging.registerDeviceForRemoteMessages();
    }

    this.createDefaultChannel();

    await this.messaging
      .getToken()
      .then(this.handleDeviceToken)
      .catch((error) => {
        console.error(error);
      });

    const options = {
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: this.onRegister,

      // (required) Called when a remote is received or opened, or local notification is opened
      onNotification: this.onNotification,

      // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
      onAction: this.onAction,

      // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
      onRegistrationError: this.onRegistrationError,

      // IOS ONLY (optional): default: all - Permissions to register.
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },

      // Should the initial notification be popped automatically
      // default: true
      popInitialNotification: true,

      /**
       * (optional) default: true
       * - Specified if permissions (ios) and token (android and ios) will requested or not,
       * - if not, you must call PushNotificationsHandler.requestPermissions() later
       * - if you are not using remote notification or do not have Firebase installed, use this:
       *     requestPermissions: Platform.OS === 'ios'
       */
      requestPermissions: false,
    };

    // DO NOT USE PushNotification.configure() INSIDE A COMPONENT, EVEN App.
    PushNotification.configure(options);
  };
}
