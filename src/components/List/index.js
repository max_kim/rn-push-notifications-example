import React, { useCallback } from 'react';
import { FlatList } from 'react-native';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { Header } from '../Header';
import { Item } from '../Item';

import {
  getCurrentNotification,
  getNotificationsList,
} from '../../store/selectors/notifications';
import { notificationsActions } from '../../store/redux/notifications';

export const List = () => {
  const data = useSelector(getNotificationsList);

  const currentNotification = useSelector(getCurrentNotification);

  const dispatch = useDispatch();

  const setCurrentNotification = useCallback(
    (notification) => {
      dispatch(
        notificationsActions.setCurrentNotification(notification.messageId),
      );
    },
    [dispatch],
  );

  const removeCurrentNotification = useCallback(
    (notification) => {
      dispatch(notificationsActions.removeNotification(notification.messageId));
    },
    [dispatch],
  );

  const renderItem = useCallback(
    ({ item }) => (
      <Item
        item={item}
        isSelected={item === currentNotification}
        setCurrentNotification={setCurrentNotification}
        removeCurrentNotification={removeCurrentNotification}
      />
    ),
    [currentNotification, removeCurrentNotification, setCurrentNotification],
  );

  const keyExtractor = useCallback((item) => item.messageId, []);

  return (
    <ListBox>
      <StyledList
        data={data}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        ListHeaderComponent={Header}
        ListFooterComponent={SeparatorComponent}
        ItemSeparatorComponent={SeparatorComponent}
        showsVerticalScrollIndicator={false}
      />
    </ListBox>
  );
};

const ListBox = styled.SafeAreaView`
  flex: 1;
  width: 100%;
`;

const StyledList = styled(FlatList).attrs(({ data }) => ({
  contentContainerStyle: {
    flex: Number(!data?.length),
  },
}))`
  width: 100%;
`;

const SeparatorComponent = styled.View`
  height: 1px;
  width: 100%;
  background: black;
`;
