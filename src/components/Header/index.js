import React, { useCallback } from 'react';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

import { notificationsActions } from '../../store/redux/notifications';
import { Alert } from 'react-native';

export const Header = () => {
  const dispatch = useDispatch();

  const removeAllNotifications = useCallback(() => {
    dispatch(notificationsActions.clearNotificationsList());
  }, [dispatch]);

  const removeAllNotificationsAlert = useCallback(() => {
    Alert.alert('Attention', 'Do you want to remove all the notifications?', [
      { text: 'Cancel', onPress: null, style: 'cancel' },
      {
        text: 'OK',
        onPress: removeAllNotifications,
      },
    ]);
  }, [removeAllNotifications]);

  return (
    <Container>
      <Text>Notifications</Text>
      <Button onPress={removeAllNotificationsAlert}>
        <ButtonTitle>Clear list</ButtonTitle>
      </Button>
    </Container>
  );
};

const Container = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-start;
  height: 64px;
  width: 100%;
  padding: 16px;
  border-bottom-width: 1px;
`;

const Text = styled.Text`
  font-weight: 700;
  font-size: 24px;
`;

const Button = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  height: 32px;
  padding: 0 16px;
  border-radius: 8px;
  background: cornflowerblue;
`;

const ButtonTitle = styled.Text`
  font-weight: 500;
  font-size: 14px;
`;
