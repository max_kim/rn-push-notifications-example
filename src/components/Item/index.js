import React, { useCallback, useMemo } from 'react';
import { Alert, Platform } from 'react-native';
import styled from 'styled-components';
import FastImage from 'react-native-fast-image';

export const Item = React.memo(
  ({ item, isSelected, setCurrentNotification, removeCurrentNotification }) => {
    const {
      notification: { body, title },
    } = item;

    const imgSource = useMemo(() => {
      const uri = Platform.select({
        android: item.notification.android?.imageUrl,
        ios: item.data.fcm_options?.image,
      });

      return uri ? { uri } : null;
    }, [item]);

    const removeNotificationAlert = useCallback(
      (notification) => {
        Alert.alert('Attention', 'Do you want to remove the notification?', [
          { text: 'Cancel', onPress: null, style: 'cancel' },
          {
            text: 'OK',
            onPress: () => removeCurrentNotification(notification),
          },
        ]);
      },
      [removeCurrentNotification],
    );

    const onPressItem = useCallback(() => {
      isSelected ? removeNotificationAlert(item) : setCurrentNotification(item);
    }, [isSelected, item, removeNotificationAlert, setCurrentNotification]);

    return (
      <Container onPress={onPressItem} isSelected={isSelected}>
        <TextBlock>
          <Title>{title}</Title>
          <Text>{body}</Text>
        </TextBlock>
        {imgSource && <ImageBox source={imgSource} />}
      </Container>
    );
  },
);

const Container = styled.TouchableOpacity`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  height: 48px;
  width: 100%;
  padding: 0 16px;
  background: ${({ isSelected }) => (isSelected ? 'lightgray' : 'transparent')};
`;

const TextBlock = styled.View`
  flex: 1;
  justify-content: center;
  height: 100%;
`;

const Title = styled.Text`
  font-weight: 700;
  font-size: 17px;
`;

const Text = styled.Text`
  font-weight: 400;
  font-size: 14px;
`;

const ImageBox = styled(FastImage)`
  height: 40px;
  width: 60px;
  border-radius: 6px;
`;
