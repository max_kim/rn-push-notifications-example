import React, { useEffect } from 'react';
import { StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import { List } from './src/components/List';

import { getStore } from './src/store';
import { NotificationService } from './src/services/notifications';
import {
  getDeviceTokenHandler,
  getNotificationInteractionHandler,
  getSaveNotificationToStoreHandler,
} from './src/store/utils';

const { store, persistor } = getStore();

const notificationsHandler = getSaveNotificationToStoreHandler();
const deviceTokenHandler = getDeviceTokenHandler();
const interactionHandler = getNotificationInteractionHandler();

const notificationsService = new NotificationService({
  notificationsHandler,
  interactionHandler,
  deviceTokenHandler,
});

// Register background handler
notificationsService.registerBackgroundMessageHandler();

const App = () => {
  useEffect(() => {
    return notificationsService.subscribeOnForegroundMessages(true);
  }, []);

  useEffect(() => {
    return notificationsService.subscribeOnTokenRefresh();
  }, []);

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <StatusBar barStyle={'light-content'} />
        <List />
      </PersistGate>
    </Provider>
  );
};

export default App;
